#include <stdio.h>
#include <stdbool.h>
#include <sys/stat.h> 
#include <sys/types.h> 
#include <fcntl.h>
#include <stdlib.h> 
#include <string.h>
#include <pthread.h>
#include <unistd.h>


#define CHUNK 1024

//for debuging
bool do_print;
//current xored chunk
char *BUFFER;
// if no input buffer is as big as chunk, write only first max_size_input_buffer chars to file
int max_size_input_buffer = 0;

//global variable so all threads could free at crash
pthread_t* input_thrs;

// xored_threads - num of threads already xored the current buffer
// relevant_threads - number of input files still relevant (long enough)
int xored_threads, relevant_threads, next_round_irrelevant_threads = 0;

//output fd needs to be reachable to all
int output_fd;

//guard writing to the buffer and global counters
pthread_mutex_t lock;
pthread_cond_t all_threads_finished_cv;

void debug(char* message) {
	if (do_print) {
		fprintf(stderr, "%s", message);
	}
}

void clean_close_objects(int status) {
	
	free(input_thrs);
	free(BUFFER);

	pthread_mutex_destroy(&lock);
	pthread_cond_destroy(&all_threads_finished_cv);

	if (status != 0) { exit(1); }

}

void* xor_to_output(void* input_path) {
	int input_fd, bytes_read = 0;
	char *INPUT_BUFFER = calloc(CHUNK, sizeof(char));
	if (INPUT_BUFFER == NULL) {
		perror("Cannot allocate memory\n");
		exit(1);
	}

	if ((input_fd = open((char*)input_path, O_RDONLY)) < 0)
	{
		perror("Cannot open input file\n");
		clean_close_objects(1);
	}

	//read buffer untill file ends
	while ((bytes_read = (int)read(input_fd, INPUT_BUFFER, CHUNK)) > 0) {
		
		if (pthread_mutex_lock(&lock) != 0) {
			perror("Error while locking thread open input file\n");
			clean_close_objects(1);
		}
		for (int byte_index = 0; byte_index < bytes_read; ++byte_index) {
			BUFFER[byte_index] ^= INPUT_BUFFER[byte_index];
		}
		memset(INPUT_BUFFER, 0, CHUNK);
		max_size_input_buffer = (max_size_input_buffer < bytes_read) ? bytes_read : max_size_input_buffer;
		xored_threads++;
		//xored all threads for this buffer
		if (relevant_threads == xored_threads) {
			if (write(output_fd, BUFFER, max_size_input_buffer) < 0) {
				perror("Error while writing to output file\n");
				clean_close_objects(1);
			}
			xored_threads = max_size_input_buffer = 0;
			memset(BUFFER, 0, CHUNK);
			//file ended, shouldn't count it in th next round
			if (bytes_read != CHUNK) {
				relevant_threads--;
			}

			relevant_threads -= next_round_irrelevant_threads;
			next_round_irrelevant_threads = 0;

			if (pthread_cond_broadcast(&all_threads_finished_cv) < 0) {
				perror("Error notifing waiting threads\n");
				clean_close_objects(1);
			}
			if (pthread_mutex_unlock(&lock) != 0) {
				perror("Error while unlocking thread open input file\n");
				clean_close_objects(1);
			}
			if (bytes_read != CHUNK) {
				break;
			}
		}
		else {
			if (bytes_read != CHUNK) {
				next_round_irrelevant_threads++;
			}
			if ((pthread_cond_wait(&all_threads_finished_cv, &lock) < 0)) {
				perror("Error waiting on cv\n");
				clean_close_objects(1);
			}
			
			if (pthread_mutex_unlock(&lock) != 0) {
				perror("Error while unlocking thread open input file\n");
				clean_close_objects(1);
			}
			if (bytes_read != CHUNK) {
				break;
			}
		}

		
	}

	if (bytes_read < 0) {
		perror("Error reading from file\n");
		clean_close_objects(1);
	}

	if ((close(input_fd)) < 0)
	{
		perror("Cannot close input file\n");
		clean_close_objects(1);
	}
	free(INPUT_BUFFER);
	return NULL;
}

int main(int argc, char ** argv)
{
	int num_threads;
	int num_input_fd = num_threads = relevant_threads = argc - 2;
	printf("Hello, creating %s from %d input files\n", argv[1], num_input_fd);
	fflush(stdout);
	if (pthread_mutex_init(&lock, NULL) != 0) {
		perror("mutex init failed\n");
		exit(1);
	}
	if (pthread_cond_init(&all_threads_finished_cv, NULL) != 0) {
		perror("condition variable init failed\n");
		exit(1);
	}
	do_print = false; xored_threads = 0; 
	BUFFER = calloc(CHUNK, sizeof(char));
	if (BUFFER == NULL) {
		perror("Cannot allocate memory\n");
		exit(1);
	}
	if (argc < 3) {
		fprintf(stderr, "User should specify at least output file and one input file");
		exit(1);
	}

	
	
	input_thrs = (pthread_t*)malloc(sizeof(pthread_t) * num_threads);
	if (input_thrs == NULL) {
		perror("Cannot allocate memory\n");
		exit(1);
	}

	if ((output_fd = open(argv[1], O_WRONLY | O_CREAT | O_TRUNC, S_IRWXG | S_IRWXU)) < 0)
	{
		perror("Cannot open output file\n"); 
		clean_close_objects(1);
	}

	
	for (int i = 0; i < num_input_fd; ++i) {
		if (pthread_create(&input_thrs[i], NULL, xor_to_output, argv[i + 2]) < 0) {
			perror("Cannot create pthread\n");
			clean_close_objects(1);
		}
	}

	for (int i = 0; i < num_input_fd; ++i) {
		if (pthread_join(input_thrs[i], NULL) < 0) {
			perror("Cannot join pthread\n");
			clean_close_objects(1);
		}
	}


	if ((close(output_fd)) < 0)
	{
		perror("Cannot close input file\n");
		clean_close_objects(1);
	}
	clean_close_objects(0);

	struct stat st;

	if (stat(argv[1], &st) < 0) {
		perror("Cannot retrieve file stats\n");
	}
	printf("Created %s with size %d bytes\n", argv[1], (int)st.st_size);
	
	return 0;


}